### React Integration with Observable Notebook


Test app to see how to create front end react web page 


add the Observable runtime and Jupyter notebook to the React app.

To install a notebook, you need the URL for the npm (and Yarn) installable tarball. You can retrieve it for this notebook by right-clicking on Download code within the notebook menu at the top of the page, and choosing to Copy link address.

-- URL TO the observable notebook
https://api.observablehq.com/@observablehq/how-to-embed-a-notebook-in-a-react-app.tgz?v=3


yarn add @observablehq/runtime@4
yarn add https://api.observablehq.com/@observablehq/how-to-embed-a-notebook-in-a-react-app.tgz?v=3