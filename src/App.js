import React, { Component } from 'react';
import {Runtime, Inspector} from "@observablehq/runtime";
// import './App.css';
import notebook from "@jashkenas/how-to-embed-a-notebook-in-a-react-app";

class App extends Component  {
  state = {
    speed: 0.1,
  };
  animationRef = React.createRef();
  
  setSpeed = event => {
    this.setState({
      speed: event.target.valueAsNumber
    });
  }
  componentDidMount() {
    const runtime = new Runtime();
    runtime.module(notebook, name => {
      if (name === "animation") {
        return new Inspector(this.animationRef.current);
      }
      // Reference to the notebook's mutable state 
      if (name === "mutable speed") {
        return {fulfilled: (value) => {
          this.animationSpeed = value;
        }};
      }
    });
  }
  componentDidUpdate(nextProps, nextState) {
    //updates the mutable speed
    if (nextState.speed !== this.state.speed) {
      this.animationSpeed.value = nextState.speed;
    }
  }
  render() {
    return (
      <div className="App">
        <div ref={this.animationRef}></div>
        <small>Speed: {this.state.speed}</small>
        <br/>
        <input 
          id="speed-slider"
          type="range" 
          min="0" 
          max="2" 
          step="0.1" 
          value={this.state.speed}
          onChange={this.setSpeed}
        />
      </div>
    );
  }
}
export default App;
